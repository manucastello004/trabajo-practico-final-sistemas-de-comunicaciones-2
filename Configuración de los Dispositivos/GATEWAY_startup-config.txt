!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname GATEWAY
!
!
!
enable secret 5 $1$mERr$V8DLh6mmDdhU2.SIzBuS2/
enable password siscom2
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO1941/K9 sn FTX1524X542-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0
 ip address 172.18.2.210 255.255.255.252
 ip nat inside
 duplex auto
 speed auto
!
interface GigabitEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Serial0/0/0
 ip address 200.2.2.18 255.255.255.252
 ip nat outside
 clock rate 2000000
!
interface Serial0/0/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 1
 log-adjacency-changes
 network 192.168.10.108 0.0.0.3 area 0
 network 172.18.2.208 0.0.0.3 area 0
 network 200.2.2.16 0.0.0.3 area 0
 default-information originate
!
ip nat inside source list 1 interface Serial0/0/0 overload
ip classless
ip route 0.0.0.0 0.0.0.0 Serial0/0/0 
!
ip flow-export version 9
!
!
access-list 1 permit 172.18.2.0 0.0.0.255
!
banner motd #
--------------------------------------------------
**************************************************
   !!!!!SOLO INGRESO PERSONAL AUTORIZADO!!!!!
**************************************************
--------------------------------------------------
             .....GRUPO 2.....
--------------------------------------------------
#
!
!
!
!
!
line con 0
 password siscom2
 login
!
line aux 0
!
line vty 0 4
 password siscom2
 login
!
!
!
end

