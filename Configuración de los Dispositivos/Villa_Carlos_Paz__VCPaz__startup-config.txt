!
version 12.2(37)SE1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname VCPaz
!
!
enable secret 5 $1$mERr$V8DLh6mmDdhU2.SIzBuS2/
enable password siscom2
!
!
!
!
!
ip routing
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/1
 switchport access vlan 10
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/2
 switchport access vlan 10
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/3
 switchport access vlan 10
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/4
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/5
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/6
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/7
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/8
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/9
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/10
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/11
 switchport access vlan 100
 switchport mode access
 switchport nonegotiate
!
interface FastEthernet0/12
!
interface FastEthernet0/13
!
interface FastEthernet0/14
!
interface FastEthernet0/15
!
interface FastEthernet0/16
!
interface FastEthernet0/17
!
interface FastEthernet0/18
!
interface FastEthernet0/19
!
interface FastEthernet0/20
!
interface FastEthernet0/21
!
interface FastEthernet0/22
!
interface FastEthernet0/23
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface FastEthernet0/24
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface GigabitEthernet0/1
 no switchport
 ip address 172.18.2.222 255.255.255.252
 duplex auto
 speed auto
!
interface GigabitEthernet0/2
 no switchport
 ip address 172.18.2.217 255.255.255.252
 duplex auto
 speed auto
!
interface Vlan1
 ip address 172.18.2.225 255.255.255.252
!
interface Vlan10
 mac-address 0050.0f31.7d01
 ip address 172.18.2.201 255.255.255.248
!
interface Vlan100
 mac-address 0050.0f31.7d02
 ip address 172.18.2.161 255.255.255.240
!
interface Vlan555
 mac-address 0050.0f31.7d03
 ip address 172.18.2.97 255.255.255.224
!
router ospf 1
 log-adjacency-changes
 network 172.18.2.96 0.0.0.31 area 0
 network 172.18.2.160 0.0.0.15 area 0
 network 172.18.2.176 0.0.0.7 area 0
 network 172.18.2.200 0.0.0.7 area 0
 network 172.18.2.216 0.0.0.3 area 0
 network 172.18.2.220 0.0.0.3 area 0
 network 172.18.2.224 0.0.0.3 area 0
!
ip classless
!
ip flow-export version 9
!
!
!
banner motd #
--------------------------------------------------
**************************************************
   !!!!!SOLO INGRESO PERSONAL AUTORIZADO!!!!!
**************************************************
--------------------------------------------------
             .....GRUPO 2.....
--------------------------------------------------
#
!
!
!
!
!
line con 0
 password siscom2
 login
!
line aux 0
!
line vty 0 4
 password siscom2
 login
line vty 5 15
 password siscom2
 login
!
!
!
!
end

